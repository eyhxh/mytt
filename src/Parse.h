/*
 * Parse.h
 *
 *  Created on: 2017-10-1
 *      Author: hxh
 */

#ifndef PARSE_H_
#define PARSE_H_

#include <string>
#include <map>
using namespace std;



#include "cJSON.h"


#include  "Device.h"


class Parse {
public:
	Parse();
	virtual ~Parse();
//
//	Parse* get()
//	{
//		static Parse *m_parse = new Parse();
//		return m_parse;
//	}

	//bool doparse(const char *str);


	bool doparse(char* str);
	string jsongetstr(string name);
	int jsongetint(string name);
	string stringify();
	string tx_hid;
	string rx_hid;
	string time;
	string cmd;
	int index;
	string msg;
	string payload;

	static string map2jsonstr(map<string, Device*> maps);
private:
	cJSON* root;
};

#endif /* PARSE_H_ */
