/*
 * Device.cpp
 *
 *  Created on: 2017-10-1
 *      Author: hxh
 */

#include "Device.h"
#include "time.h"
#include "unistd.h"
#include "stdio.h"
#include "string.h"

Device::~Device() {

}

Device::Device(int sock, string hid) {
	this->sock = sock;
	this->hid = hid;
	this->lastCommunicationTime = time(NULL);  //新建设备的时候，最后一次通信时间就是创建的时间
	mNetStatus = offline;
	mWorkStatus = on;
}


bool Device::isAlive() {
	return true;
}

bool Device::sendack(int index) {
	char buf[36]={0};
	snprintf(buf,36, "{\"index\":%d,\"msg\":\"succ\"}",index);

	int ret = write(this->sock,buf,strlen(buf));
	if(ret == strlen(buf))
		return true;
	else
		return false;
}

int Device::sendmsg(string msg) {
	return write(sock,msg.c_str(),msg.length());
}

void Device::updateStatus() {

	this->lastCommunicationTime = time(NULL);
}
