/*
 * DevManager.h
 *
 *  Created on: 2017-10-1
 *      Author: hxh
 */

#ifndef DEVMANAGER_H_
#define DEVMANAGER_H_

#include "Device.h"
#include <map>
using namespace std;




class DevManager {
public:
	DevManager();
	virtual ~DevManager();

	static DevManager* get()
	{
		static DevManager *m_devman = new DevManager();
		return m_devman;

	}

	bool update(Device *dev);
	string getlist();
	Device *getdevice(string hid);
	bool removeDevice(int fd);
	bool removeDevice(string hid);
private:
	map<string , Device*> m_devices ;
	map<string , Device*>::iterator device_iter;

	map<int ,string> m_fd2hid;
};

#endif /* DEVMANAGER_H_ */
