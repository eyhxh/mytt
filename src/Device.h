/*
 * Device.h
 *
 *  Created on: 2017-10-1
 *      Author: hxh
 */

#ifndef DEVICE_H_
#define DEVICE_H_
#include <string>

#include <iostream>
using namespace std;




/**
 * 1、设备是否在线
 * 2、
 *
 *
 */

enum DEV_NET_STATUS{
	online = 1,
	offline,
	error
};

//TODO 这里肯定有一个状态同步的问题！！！
enum DEV_WORK_STATUS{
	on = 1, //打开状态
	off  //关闭状态
};
class Device {
public:
	Device(int sock, string hid);

	virtual ~Device();
	bool isAlive();
	void updateStatus();
	bool sendack(int index);
	const string& getHid() const {
		return hid;
	}

	string getLastCommunicationTime() const {
		tm* local;
		local = localtime(&lastCommunicationTime);
		char timestr[128]={0};
		strftime(timestr,64,"%Y-%m-%d %H:%M:%S", local);
		//cout << timestr << endl;
		return timestr;
	}

	DEV_NET_STATUS getNetStatus() const {

		return mNetStatus;
	}

	DEV_WORK_STATUS getWorkStatus() const {
		return mWorkStatus;
	}

	int getSock() const {
		return sock;
	}

	const string& getIpaddr() const {
		return ipaddr;
	}

	void setIpaddr(const string& ipaddr) {
		this->ipaddr = ipaddr;
	}
	int sendmsg(string msg);


private:
	int sock;
	string hid;
	time_t lastCommunicationTime; //TODO 没做 最后上线时间，这个其实很重要
	string ipaddr;
	DEV_NET_STATUS mNetStatus;

	DEV_WORK_STATUS mWorkStatus;

};

#endif /* DEVICE_H_ */
