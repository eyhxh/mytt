# 说明
这是一个tcp的数据转发服务器，每个设备有一个ID号，在连接上服务器的时候发送注册命令注册，服务器会维护在线设备列表，可以通过查询命令查找在线的设备

# 设备登录

{
  "cmd": "login",
  "tx_hid": "HID001",
  "index": 1
}

# 发送消息
{
  "cmd": "tx",
  "tx_hid": "HID001",
  "rx_hid": "HID002",
  "msg":"%s",
  "index": 1
}

# 设备发送心跳包
{
  "cmd": "ping",
  "tx_hid": "HID001",
  "rx_hid": "HID002",
  "index": 1
}